lazy val root = (project in file("."))
  .enablePlugins(PlayScala)
  .settings(
    name := """play-scala-hello-world-tutorial""",
    organization := "com.example",
    version := "1.0-SNAPSHOT",
    scalaVersion := "2.13.4",
    libraryDependencies ++= Seq(
      guice,
      "org.scalatestplus.play" %% "scalatestplus-play" % "5.0.0" % Test
    ),
    scalacOptions ++= Seq(
      "-feature",
      "-deprecation",
      "-Xfatal-warnings"
    )
  )

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", "MANIFEST.MF") => MergeStrategy.discard
  case PathList("jackson-annotations-2.10.5.jar", xs @ _*) => MergeStrategy.last
  case PathList("jackson-core-2.10.5.jar", xs @ _*) => MergeStrategy.last
  case PathList("jackson-databind-2.10.5.jar", xs @ _*) => MergeStrategy.last
  case PathList("jackson-dataformat-cbor-2.10.5.jar", xs @ _*) => MergeStrategy.last
  case PathList("jackson-datatype-jdk8-2.10.5.jar", xs @ _*) => MergeStrategy.last
  case PathList("jackson-datatype-jsr310-2.10.5.jar", xs @ _*) => MergeStrategy.last
  case PathList("jackson-module-parameter-names-2.10.5.jar", xs @ _*) => MergeStrategy.last
  case PathList("jackson-module-paranamer-2.10.5.jar", xs @ _*) => MergeStrategy.last
  case _ => MergeStrategy.first
}
